﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Assignment3
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISongDetails" in both code and config file together.
    [ServiceContract]
    public interface ISongDetails
    {
        [OperationContract]
        [WebGet(UriTemplate = "/songs/{song}")]
        List<string> Song(string song);
    }

    [DataContract]
    public class Song
    {
        [DataMember]
        public string SongName { get; set; }

        [DataMember]
        public string Genre { get; set; }

        [DataMember]
        public string Artist { get; set; }

        [DataMember]
        public string Album { get; set; }

        [DataMember]
        public List<Song> Songs { get; set; }
    }
}