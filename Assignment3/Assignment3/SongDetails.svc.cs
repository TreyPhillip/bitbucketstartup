﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Assignment3
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SongDetails" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SongDetails.svc or SongDetails.svc.cs at the Solution Explorer and start debugging.
    public class SongDetails : ISongDetails
    {
        public List<string> Song(string song)
        {

            Song Test = new Song
            {
                SongName = "TestName",
                Album = "TestAlbum",
                Genre = "TestGenre",
                Artist = "TestArtist"
            };
            Song DanceMonkey = new Song
            {
                SongName = "Dance Monkey",
                Album = "The Kids Are Coming",
                Genre = "Alternative/Indie",
                Artist = "Tones and I"
            };
            Song LoseYouToLoveMe = new Song
            {
                SongName = "Lose You To Love Me",
                Album = "Single",
                Genre = "Pop",
                Artist = "Selena Gomez"
            };
            Song Memories = new Song
            {
                SongName = "Memories",
                Album = "Single",
                Genre = "Pop",
                Artist = "Maroon 5"
            };



            if (song == "TestName")
            {
                List<string> Songs = new List<string>();

                Songs.Add(Test.SongName);
                Songs.Add(Test.Genre);
                Songs.Add(Test.Album);
                Songs.Add(Test.Artist);

                return Songs;
            }
            else if (song == "Dance Monkey")
            {
                List<string> Songs = new List<string>();

                Songs.Add(DanceMonkey.SongName);
                Songs.Add(DanceMonkey.Genre);
                Songs.Add(DanceMonkey.Album);
                Songs.Add(DanceMonkey.Artist);

                return Songs;
            }
            else if (song == "Lose You To Love Me")
            {
                List<string> Songs = new List<string>();

                Songs.Add(LoseYouToLoveMe.SongName);
                Songs.Add(LoseYouToLoveMe.Genre);
                Songs.Add(LoseYouToLoveMe.Album);
                Songs.Add(LoseYouToLoveMe.Artist);

                return Songs;
            }
            else if (song == "Memories")
            {
                List<string> Songs = new List<string>();

                Songs.Add(Memories.SongName);
                Songs.Add(Memories.Genre);
                Songs.Add(Memories.Album);
                Songs.Add(Memories.Artist);

                return Songs;
            }
            else
                return null;
            
        }
    }
}


